import java.util.Arrays;

public class driverClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UpperTrianglerMatrix cs = new UpperTrianglerMatrix();
		cs.setElement(0, 0, "damn");
		cs.setElement(0, 1, "life2");
		cs.setElement(0, 2, "life3");
		cs.setElement(0, 3, "What");
		cs.setElement(1, 1, "Working");
		cs.setElement(1, 2, "life");
		cs.setElement(1, 3, "Cats");
		cs.setElement(2, 2, "Last");
		cs.setElement(2, 3, "baseball");
		cs.setElement(3, 3, "iefjsdhj");

		/*
		 * Shows the that the set, get, and ToString.
		 */
		System.out.println(cs.ToString());
		UpperTrianglerMatrix cs3 = new UpperTrianglerMatrix();
		cs3.setElement(0, 0, "damn");
		cs3.setElement(0, 1, "life2");
		cs3.setElement(0, 2, "life3");
		cs3.setElement(0, 3, "What");
		cs3.setElement(1, 1, "Working");
		cs3.setElement(1, 2, "life");
		cs3.setElement(1, 3, "Cats");
		cs3.setElement(2, 2, "Last");
		cs3.setElement(2, 3, "baseball");
		cs3.setElement(3, 3, "iefjsdhj");

		/*
		 * Showing that equals works.
		 */
		System.out.println(cs.equals(cs3));

		cs3.setElement(0, 0, "changed");
		System.out.println(cs.equals(cs3));
		things t = new things();
		System.out.println(cs.equals(t));

		UpperTrianglerMatrix cs4 = largestCombo(cs, cs3);

		/*
		 * Shows that the largest combo works
		 */
		System.out.println(cs4.ToString());

		/*
		 * Shows that the clone function works.
		 */

		UpperTrianglerMatrix cs5 = cs.Clone();
		System.out.println(cs5.ToString());
		cs5.setElement(0, 0, "happy");
		System.out.println(cs5.ToString());
		System.out.println(cs.ToString());
		/*
		 * Shows that the concatenateUTMs method works.
		 */

		UpperTrianglerMatrix cs6 = concatenateUTMs(cs, cs4);
		System.out.println(cs6.ToString());

	}
	
	/*
	 * This method called largestCombo takes in two UpperTrianglerMatrix. The longest strings of the two matrixes are 
	 *  into all the cells of the new UpperTrianglerMatrix.
	 */

	public static UpperTrianglerMatrix largestCombo(UpperTrianglerMatrix one, UpperTrianglerMatrix two) {

		UpperTrianglerMatrix newUTM = new UpperTrianglerMatrix();
		String temp[] = newUTM.getUTM();
		String front[] = one.getUTM();
		String back[] = two.getUTM();
		String large = front[0];

		for (int i = 0; i < temp.length; i++) {

			if (large.length() < front[i].length())
				large = front[i];

		}
		for (int i = 0; i < temp.length; i++) {

			if (large.length() < back[i].length())
				large = back[i];

		}

		for (int i = 0; i < temp.length; i++) {

			temp[i] = large;

		}

		newUTM.setUTM(temp);

		return newUTM;

	} 
	
	/*
	 * This methods takes two UpperTrianglerMatrix and take the strings from both matrix and combine them into the 
	 * new UpperTrianglerMatrix.
	 */

	public static UpperTrianglerMatrix concatenateUTMs(UpperTrianglerMatrix one, UpperTrianglerMatrix two) {
		UpperTrianglerMatrix newUTM = new UpperTrianglerMatrix();
		String temp[] = newUTM.getUTM();
		String front[] = one.getUTM();
		String back[] = two.getUTM();

		for (int i = 0; i < front.length; i++) {

			temp[i] = front[i] + " " + back[i];
		}

		newUTM.setUTM(temp);

		return newUTM;

	}

}
