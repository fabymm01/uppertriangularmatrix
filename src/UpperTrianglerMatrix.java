import java.util.Arrays;

public class UpperTrianglerMatrix implements Cloneable {
	private  String UTM[];


/*This method sets up the string array for the matrix
 */
	public UpperTrianglerMatrix() {

	 UTM = new String[11];
	 
	 
	 for (int i =0 ;i<UTM.length;i++) {
		 
		 
		 UTM[i]="";
		 
		 
		 
	 }
	 
	 

	}
	/*
	 * This method uses a for loop to print out the matrix
	 */

	public String ToString() {
		String re = "";

		int i = 0;

		while (i <= 3) {
			int j = 0;
			re = re + "|";

			while (j <=3) {

				re = re + getElement(i, j) + ",";
				j++;
			}

			re = re + "|\n";

			i++;
		}

		return re;

	}
	/*This makes sure that these objects are equal and that the object is a instance 
	 * of the Uppertriangler matrix.
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
public boolean equals(Object o) {
	
	
	if (o instanceof UpperTrianglerMatrix) {
	//System.out.println("It is this type of object.");
	if ((((UpperTrianglerMatrix) o).ToString()).equals(ToString())) {
		
		
		
		return true;
	}}
	
	
return false;
	
	
	
	
}

	public void setElement(int row, int col, String data) {
		if (row <=col) {
			int index = (4*row)+col-((row*(row+1))/2);
			//System.out.println(index);
			UTM[index] = data;
		} else {

		}

	}

	public String getElement(int row, int col) {
		String temp = null;

		if (row <=col)
			temp = UTM[(4*row)+col-((row*(row+1))/2)];

		return temp;
	}

	public String[] getUTM() {
		return UTM;
	}

	public void setUTM(String[] uTM) {
		UTM = uTM;
	}
	
	
/*clones the object
 * 
 */

	public UpperTrianglerMatrix Clone() {
		UpperTrianglerMatrix temp;
		try {
			temp = (UpperTrianglerMatrix) super.clone();
			
			
			temp.setUTM(Arrays.copyOf(getUTM(), getUTM().length));
			
			//System.out.println(temp.toString());
			return temp;

		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
		
		
		

		
	}

}
